# WsInmobiliario
Web service para consulta en base de datos inmobiliarias.

## Estructura:
 * server/: Server soap utilizando [NuSoap](https://sourceforge.net/projects/nusoap/), en una versión compatible con php 5.3
 * cliente/: 
 -- nusoapclient.php: Cliente nusoap
 -- soapclient.php: Cliente soap (cliente nativo de soap de php)

## Librerías:

* [NuSoap](https://sourceforge.net/projects/nusoap/).
* [AdoDb](https://adodb.org/dokuwiki/doku.php).

## Versiones:
* Php: 5.5.54
* Apache/2.2.22
* MySQL: 5.5.54

## Para Probar:

Se puede utilizar [SoapTest](http://www.soapclient.com/soaptest.html) y especificando la dirección del ws de desarrollo:
```
http://ws.tres-erres.com.ar/app/server/index.php?wsdl
```
o, ingresar directamente al mini cliente:

```
http://ws.tres-erres.com.ar/app/client
```

