<?php
require_once("./libs/nusoap/nusoap.php");
require_once("./libs/adodb/adodb.inc.php");
define('ADODB_ERROR_LOG_TYPE',3); 
define('ADODB_ERROR_LOG_DEST','./cache/errormysql.txt');
date_default_timezone_set ("America/Argentina/Buenos_Aires");
include('./libs/adodb/adodb-errorhandler.inc.php');
require_once("./wsconnect.php");
require_once("./wsqueries.php");
require_once("./libs/wslibs.php");

$Version = 3;

$server = new soap_server();
$server->configureWSDL("wsInmobiliario");
//$server->debug_flag=true;
$server->wsdl->schemaTargetNamespace = $namespace;

//Definicion respuesta
$server->wsdl->addComplexType('respuesta','complexType','struct','all','',
array('Version'=>array('name'=>'Version','type' => 'xsd:string'),
'Tipo' => array('name'=>'Tipo','type' => 'xsd:string'),
'Valor' => array('name'=>'Valor','type' => 'xsd:string'),
'Mensaje' => array('name'=>'Mensaje','type'=>'tns:mensaje'),
'Cambios' => array('name'=>'Cambios','type'=>'tns:cambios'),
'Bajas' => array('name'=>'Bajas','minOccurs'=>0,'type'=>'tns:bajas'),
'Inmuebles' => array('name'=>'Inmuebles','minOccurs'=>0,'type'=>'tns:inmuebles')));

// Mensaje
$server->wsdl->addComplexType('mensaje','complexType','struct','all','',
array('Codigo_Mensaje'=>array('name'=>'Codigo_Mensaje','type' => 'xsd:string'),
'Texto_Mensaje' => array('name'=>'Texto_Mensaje','type' => 'xsd:string'),
'Fecha_Mensaje' => array('name'=>'Fecha_Mensaje','type' => 'xsd:string'),
'Hora_Mensaje' => array('name'=>'Hora_Mensaje','type' => 'xsd:string')));

// cambios
$server->wsdl->addComplexType('cambios','complexType','array','','SOAP-ENC:Array',
array(),
	array('Inmueble'=>array('name'=>'Inmueble','type'=>'tns:inmueble')));

// bajas
$server->wsdl->addComplexType('bajas','complexType','array','','SOAP-ENC:Array',
array(),
	array('Inmueble'=>array('name'=>'Inmueble','type'=>'tns:inmueble')));

// inmuebles
$server->wsdl->addComplexType('inmuebles','complexType','array','','SOAP-ENC:Array',
array(),
	array('Inmueble'=>array('name'=>'Inmueble','type'=>'tns:inmueble')));

// Inmueble
$server->wsdl->addComplexType('inmueble','complexType','struct','all','',
array('Matricula'=>array('name'=>'Matricula','type' => 'tns:matricula'),
'Lote'=>array('name'=>'Lote','type' =>'xsd:string'),
'Fraccion'=>array('name'=>'Fraccion','type' =>'xsd:string'),
'Manzana'=>array('name'=>'Manzana','type' =>'xsd:string'),
'Chacra'=>array('name'=>'Chacra','type' =>'xsd:string'),
'Quinta'=>array('name'=>'Quinta','type' =>'xsd:string'),
'Nomenclatura'=>array('name'=>'Nomenclatura','type' =>'xsd:string'),
'Numero_Plano'=>array('name'=>'Numero_Plano','type' =>'xsd:string'),
'Tomo_Plano'=>array('name'=>'Tomo_Plano','type' =>'xsd:string'),
'Folio_Plano'=>array('name'=>'Folio_Plano','type' =>'xsd:string'),
'Unidad_Complementaria'=>array('name'=>'Unidad_Complementaria','type'=>'tns:unidadcomplementaria'),
'Titular'=>array('name'=>'Titular','type'=>'tns:titular')));

// Matricula
$server->wsdl->addComplexType('matricula','complexType','struct','all','',
array('Codigo_Departamento'=>array('name'=>'Codigo_Departamento','type' => 'xsd:string'),
'Numero'=>array('name'=>'Numero','type' => 'xsd:string'),
'Unidad_Funcional'=>array('name'=>'Unidad_Funcional','type' => 'xsd:string')));

$server->wsdl->addComplexType('unidadcomplementaria','complexType','array','','SOAP-ENC:Array',
array(),
	array('Denominacion_UC'=>array('name'=>'Denominacion_UC','type' => 'xsd:string'),
		'Porcentaje_UC'=>array('name'=>'Porcentaje_UC','type' => 'xsd:string'),
		'Nomenclatura_UC'=>array('name'=>'Nomenclatura_UC','type' => 'xsd:string')));

// Titular
$server->wsdl->addComplexType('titular','complexType','array','','SOAP-ENC:Array',
array(),
array('Tipo_Documento'=>array('name'=>'Tipo_Documento','type' => 'xsd:string'),
'Numero_Documento'=>array('name'=>'Numero_Documento','type' => 'xsd:string'),
'Apellido'=>array('name'=>'Apellido','type' => 'xsd:string'),
'Nombres'=>array('name'=>'Nombres','type' => 'xsd:string'),
'Estado_Civil'=>array('name'=>'Estado_Civil','type' => 'xsd:string'),
'Fecha_Nacimiento'=>array('name'=>'Fecha_Nacimiento','type' => 'xsd:string'),
'Nupcias'=>array('name'=>'Nupcias','type' => 'xsd:string'),
'CUIT'=>array('name'=>'CUIT','type' => 'xsd:string'),
'Personeria_Juridica'=>array('name'=>'Personeria_Juridica','type' => 'xsd:string'),
'Porcentaje_Dominio_Numerador'=>array('name'=>'Porcentaje_Dominio_Numerador','type' => 'xsd:string'),
'Porcentaje_Dominio_Denominador'=>array('name'=>'Porcentaje_Dominio_Denominador','type' => 'xsd:string'),
'Fecha_Escritura'=>array('name'=>'Fecha_Escritura','type' => 'xsd:string'),
'Fecha_Fin'=>array('name'=>'Fecha_Fin','type' => 'xsd:string'),
'Fecha_Registro'=>array('name'=>'Fecha_Registro','type' => 'xsd:string')));


//wsrpi: Metodo para traer datos por defecto... Ver tema encoded / literal...
$server->register('wsrpi',array('Tipo'=>'xsd:string','Parametro'=>'xsd:string', 'Usuario'=>'xsd:string'),array('Consulta'=>'tns:respuesta'),$namespace,false,'rpc','encoded','Consulta Inmobiliario');

$server->register('ping',array('nombre'=>'xsd:string'),array('return'=>'xsd:string'),$namespace,false,'rpc','encoded','Metodo Ping y Pong');

function ping($nombre)
{
  return "Pong " . $nombre;
}


function wsrpi($tipo,$parametro="",$user="") {
	global $okBd, $Version, $allowed_ips, $debugMode;
	$datos=array(
		'Tipo'=>$tipo,
		'Parametro'=>$parametro,
		'Usuario'=>$user
	);
	$ip=getRealIpAddr();
	$aMensajes=array(
		1=>'Consulta ejecutada correctamente',
		2=>'El inmueble solicitado: Matricula {Matricula} no ha sido encontrado en la base de datos',
		3=>'El inmueble solicitado: Nomenclatura {Nomenclatura} no ha sido encontrado en la base de datos',
		4=>'Los datos solicitados del CUIT {CUIT} no han sido encontrados en la base de datos',
		5=>'Solo puede solicitarse el lote de novedades del día de la fecha',
		6=>'Tipo de Consulta inválida',
		7=>'Falta el parámetro para el tipo de consulta solicitada',
		8=>'La base de datos devuelve un error al ejecutar la consulta',
		9=>'No se han enviado los parámetros necesarios para ejecutar la consulta',
		95=>'Intento acceso no autorizado',
		98=>'No se obtuvo respuesta del Web Service');

	$posibles=array('M','N','C','A','O','L');

	$complementaria_blanca=array('Denominacion_UC'=>'','Porcentaje_UC'=>0,'Nomenclatura_UC'=>'');

	$camposConsulta=array('M'=>array('Campo'=>'Matricula','Error'=>2),
		'N'=>array('Campo'=>'Nomenclatura','Error'=>3),
		'C'=>array('Campo'=>'CUIT','Error'=>4),
		'A'=>array('Campo'=>'Matricula','Error'=>2),
		'O'=>array('Campo'=>'Nomenclatura','Error'=>3));

	$inmueble_blanco=array('Matricula'=>array('Codigo_Departamento'=>0,'Numero'=>0,'Unidad_Funcional'=>''),'Lote'=>'','Fraccion'=>'','Manzana'=>'','Chacra'=>'','Quinta'=>'','Nomenclatura'=>'','Numero_Plano'=>'',
		'Tomo_Plano'=>'','Folio_Plano'=>'');
	$titular_blanco=array('Tipo_Documento'=>null,'Numero_Documento'=>'','Apellido'=>'','Nombres'=>'','Estado_Civil'=>'','Fecha_Nacimiento'=>'',
		'Nupcias'=>'','CUIT'=>'','Personeria_Juridica'=>'','Porcentaje_Dominio_Numerador'=>'','Porcentaje_Dominio_Denominador'=>'',
		'Fecha_Escritura'=>'','Fecha_Fin'=>'','Fecha_Registro'=>'');
	if (in_array($ip,$allowed_ips)) {
		if ((!empty($datos['Tipo'])) and (in_array($datos['Tipo'],$posibles))) {
			if ($okBd) {
				if (($datos['Tipo']=='L') and (empty($datos['Parametro']))) {
					$datos['Parametro']=date("Y-m-d");
				}
				if (empty($datos['Parametro'])) {

					$respuesta=array('Version'=>$Version,'Tipo'=>"X",'Valor'=>"",'Mensaje'=>array('Codigo_Mensaje'=>7,'Texto_Mensaje'=>$aMensajes[7],'Fecha_Mensaje'=>date("Y-m-d"),'Hora_Mensaje'=>date("H:i:s")));

				} else {

					$respuesta=array('Version'=>$Version,'Tipo'=>$datos['Tipo'],'Valor'=>$datos['Parametro']);

					switch ($datos['Tipo']) {
						case 'M':
						case 'N':
						case 'C':
						case 'A':
						case 'O':
							$campo=$camposConsulta[$datos['Tipo']]['Campo'];
							$error=$camposConsulta[$datos['Tipo']]['Error'];
							if (($datos['Tipo']=='A') or ($datos['Tipo']=='O')) {
								$inmuebles=queryHistoricos($campo,$datos['Parametro']);
							} else {
								$inmuebles=queryVigentes($campo,$datos['Parametro']);
							}
							if (!$inmuebles) {
								$respuesta['Mensaje']=array('Codigo_Mensaje'=>$error,'Texto_Mensaje'=>str_replace('{' . $campo . '}',$datos['Parametro'],$aMensajes[$error]),'Fecha_Mensaje'=>date("Y-m-d"),'Hora_Mensaje'=>date("H:i:s"));
							} else {
								$respuesta['Mensaje']=array('Codigo_Mensaje'=>1,'Texto_Mensaje'=>$aMensajes[1],'Fecha_Mensaje'=>date("Y-m-d"),'Hora_Mensaje'=>date("H:i:s"));
								$matricula=$cambio=false;
								$titulares=array();
								foreach($inmuebles as $k=>$v) {
									if (!$matricula) $matricula=$v['Matricula'];
									if ($matricula<>$v['Matricula']) {
										$complementarias=queryComplementarias($matricula);
										if (!$complementarias) $complementarias=array($complementaria_blanca);
										$cambio['Unidad_Complementaria']=$complementarias;
										$cambio['Titulares']=$titulares;
										$respuesta['Inmuebles'][]=$cambio;
										$cambio=false;
										$titulares=array();
									}
									$cambio=array_intersect_key($v,$inmueble_blanco);
									$Matricula=generaMatricula($v);
									$cambio['Matricula']=$Matricula;
									$titulares[]=array_intersect_key($v,$titular_blanco);
									$matricula=$v['Matricula'];
								}
								if ($cambio) {
									$complementarias=queryComplementarias($matricula);
									if (!$complementarias) $complementarias=array($complementaria_blanca);
									$cambio['Unidad_Complementaria']=$complementarias;
									$cambio['Titulares']=$titulares;
									$respuesta['Inmuebles'][]=$cambio;
								}
							}
						break;
						case 'L':
							if (($datos['Parametro'] <> date("Y-m-d")) and ((empty($debugMode)) or (!$debugMode))) {
								$respuesta=array('Version'=>$Version,'Tipo'=>"L",'Valor'=>$datos['Parametro'],'Mensaje'=>array('Codigo_Mensaje'=>5,'Texto_Mensaje'=>$aMensajes[5],'Fecha_Mensaje'=>date("Y-m-d"),'Hora_Mensaje'=>date("H:i:s")));
							} else {
								$respuesta['Mensaje']=array('Codigo_Mensaje'=>1,'Texto_Mensaje'=>$aMensajes[1],'Fecha_Mensaje'=>date("Y-m-d"),'Hora_Mensaje'=>date("H:i:s"));
								$cambios=queryVigentesFecha($datos['Parametro']);
								if (!$cambios) $respuesta['Cambios']=array($inmueble_blanco);
								else {
									$matricula=$cambio=false;
									$titulares=array();
									foreach($cambios as $k=>$v) {
										if (!$matricula) $matricula=$v['Matricula'];
										if ($matricula<>$v['Matricula']) {
											$complementarias=queryComplementarias($matricula);
											if (!$complementarias) $complementarias=array($complementaria_blanca);
											$cambio['Unidad_Complementaria']=$complementarias;
											$cambio['Titulares']=$titulares;
											$respuesta['Cambios'][]=$cambio;
											$cambio=false;
											$titulares=array();
										}
										$cambio=array_intersect_key($v,$inmueble_blanco);
										$Matricula=generaMatricula($v);
										$cambio['Matricula']=$Matricula;
										$titulares[]=array_intersect_key($v,$titular_blanco);
										$matricula=$v['Matricula'];
									}
									if ($cambio) {
										$complementarias=queryComplementarias($matricula);
										if (!$complementarias) $complementarias=array($complementaria_blanca);
										$cambio['Unidad_Complementaria']=$complementarias;
										$cambio['Titulares']=$titulares;
										$respuesta['Cambios'][]=$cambio;
									}
								}

								$bajas=queryHistoricosFecha($datos['Parametro']);
								if (!$bajas) $respuesta['Bajas']=array($inmueble_blanco);
								else {
									$matricula=$baja=false;
									$titulares=array();
									foreach($bajas as $k=>$v) {
										if (!$matricula) $matricula=$v['Matricula'];
										if ($matricula<>$v['Matricula']) {
											$complementarias=queryComplementarias($matricula);
											if (!$complementarias) $complementarias=array($complementaria_blanca);
											$baja['Unidad_Complementaria']=$complementarias;
											$baja['Titulares']=$titulares;
											$respuesta['Bajas'][]=$baja;
											$baja=false;
											$titulares=array();
										}
										$baja=array_intersect_key($v,$inmueble_blanco);
										$Matricula=generaMatricula($v);
										$baja['Matricula']=$Matricula;
										$titulares[]=array_intersect_key($v,$titular_blanco);
										$matricula=$v['Matricula'];
									}
									if ($baja) {
										$complementarias=queryComplementarias($matricula);
										if (!$complementarias) $complementarias=array($complementaria_blanca);
										$baja['Unidad_Complementaria']=$complementarias;
										$baja['Titulares']=$titulares;
										$respuesta['Bajas'][]=$baja;
									}
								}


							}
						break;
					}
				}
			} else {
				$respuesta=array('Version'=>$Version,'Tipo'=>"X",'Valor'=>"",'Mensaje'=>array('Codigo_Mensaje'=>8,'Texto_Mensaje'=>$aMensajes[8],'Fecha_Mensaje'=>date("Y-m-d"),'Hora_Mensaje'=>date("H:i:s")));
			}
		} else {
			$respuesta=array('Version'=>$Version,'Tipo'=>"X",'Valor'=>"",'Mensaje'=>array('Codigo_Mensaje'=>6,'Texto_Mensaje'=>$aMensajes[6],'Fecha_Mensaje'=>date("Y-m-d"),'Hora_Mensaje'=>date("H:i:s")));
		}
	} else {
		$respuesta=array('Version'=>$Version,'Tipo'=>"X",'Valor'=>"",'Mensaje'=>array('Codigo_Mensaje'=>95,'Texto_Mensaje'=>$aMensajes[95],'Fecha_Mensaje'=>date("Y-m-d"),'Hora_Mensaje'=>date("H:i:s")));
	}
	$registros=logRespuesta($respuesta,$datos,$ip);
	return new soapval('return','tns:respuesta',$respuesta);
}

$POST_DATA=file_get_contents("php://input");
$server->service($POST_DATA);
exit();
