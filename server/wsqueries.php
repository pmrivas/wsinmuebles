<?php
/********************************
**    wsqueries.php: Queries.....
*********************************/

function queryVigentes($campo,$valor) {
	global $conn1;
	$rpta=$conn1->GetAll('Select * from vigentes where ' . $campo . '=? order by Matricula',array($valor));
	foreach($rpta as $k=>$v) {
		foreach($v as $kk=>$vv) {
			$rpta[$k][$kk]=trim($vv);
		}
		if ($v['Tomo_Plano']=='0') $rpta[$k]['Tomo_Plano']="";
		if ($v['Folio_Plano']=='0') $rpta[$k]['Folio_Plano']="";
		if ($v['Nupcias']=='0') $rpta[$k]['Nupcias']="";
	}
	return $rpta;
}
function queryHistoricos($campo,$valor) {
	global $conn1;
	$rpta = $conn1->GetAll('Select * from historicos where ' . $campo . '=?',array($valor));
	foreach($rpta as $k=>$v) {
		foreach($v as $kk=>$vv) {
			$rpta[$k][$kk]=trim($vv);
		}
		if ($v['Tomo_Plano']=='0') $rpta[$k]['Tomo_Plano']="";
		if ($v['Folio_Plano']=='0') $rpta[$k]['Folio_Plano']="";
		if ($v['Nupcias']=='0') $rpta[$k]['Nupcias']="";
	}
	return $rpta;
}
function queryVigentesFecha($valor) {
	global $conn1;
	$rpta=$conn1->GetAll('Select * from vigentes where Fecha_Registro=? and Fecha_Fin=? order by Matricula',array($valor,"0000-00-00"));
	foreach($rpta as $k=>$v) {
		foreach($v as $kk=>$vv) {
			$rpta[$k][$kk]=trim($vv);
		}
		if ($v['Tomo_Plano']=='0') $rpta[$k]['Tomo_Plano']="";
		if ($v['Folio_Plano']=='0') $rpta[$k]['Folio_Plano']="";
		if ($v['Nupcias']=='0') $rpta[$k]['Nupcias']="";
	}
	return $rpta;
}
function queryHistoricosFecha($valor) {
	global $conn1;
	$rpta = $conn1->GetAll('Select * from historicos where Fecha_Registro=? order by Matricula',array($valor));
	foreach($rpta as $k=>$v) {
		foreach($v as $kk=>$vv) {
			$rpta[$k][$kk]=trim($vv);
		}
		if ($v['Tomo_Plano']=='0') $rpta[$k]['Tomo_Plano']="";
		if ($v['Folio_Plano']=='0') $rpta[$k]['Folio_Plano']="";
		if ($v['Nupcias']=='0') $rpta[$k]['Nupcias']="";
	}
	return $rpta;
}

function queryComplementarias($matricula) {
	global $conn1;
	$rpta= $conn1->GetAll('Select Denominacion_UC,Porcentaje_UC,Nomenclatura_UC from complementarias where Matricula=?',array($matricula));
	foreach($rpta as $k=>$v) {
		foreach($v as $kk=>$vv) {
			$rpta[$k][$kk]=trim($vv);
		}
		if ($v['Porcentaje_UC']=='0') $rpta[$k]['Porcentaje_UC']="";
	}
	return $rpta;
}
function logRespuesta($respuesta,$datos,$ip) {
	global $conn1;
	if (empty($datos['Tipo'])) $datos['Tipo']="N";
	if (empty($datos['Parametro'])) $datos['Parametro']="NADA";
	$conn1->Execute('insert into log (Fecha,Hora,Mensaje,Origen,Tipo,Valor,Usuario,Test) values (?,?,?,?,?,?,?,?)',
		array(date("Y-m-d"),date("H:i:s"),$respuesta['Mensaje']['Codigo_Mensaje'],$ip,$datos['Tipo'],$datos['Parametro'],$datos['Usuario'],0));
	return $conn1->affected_rows();	
}