<?php
header("Expires:Fri, Jun 12 1981 08:20:00 GMT");
header("Pragma:no-cache");
header("Cache-Control:no-cache");
header('Content-Type: text/html; charset=UTF-8');
$conn1 = ADONewConnection($drivermysql);
if (!$conn1->Connect($host, $user, $password, $database)) {
	$okBd=false;
} else {
	$okBd=true;
}
$conn1->SetFetchMode(ADODB_FETCH_ASSOC);

function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
	else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
	return $ip;
}
function generaMatricula($inmueble) {
	$Matricula=array('Codigo_Departamento'=>substr($inmueble['Matricula'],0,2)*1,'Numero'=>substr($inmueble['Matricula'],2,8)*1);
	if (strlen($inmueble['Matricula'])>10) {
		$Matricula['Unidad_Funcional']=substr($inmueble['Matricula'],10)*1;
	} else {
		$Matricula['Unidad_Funcional']="";
	}
	return $Matricula;
}